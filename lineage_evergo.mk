# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from evergo device
$(call inherit-product, $(LOCAL_PATH)/device.mk)

PRODUCT_BRAND := xiaomi
PRODUCT_DEVICE := evergo
PRODUCT_MANUFACTURER := xiaomi
PRODUCT_NAME := lineage_evergo
PRODUCT_MODEL := POCO M4 Pro 5G

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi
TARGET_VENDOR := xiaomi
TARGET_VENDOR_PRODUCT_NAME := evergo
PRODUCT_BUILD_PROP_OVERRIDES += PRIVATE_BUILD_DESC="evergo-user 12 SP1A.210812.016 V13.0.7.0.SGBINXM release-keys"

# Set BUILD_FINGERPRINT variable to be picked up by both system and vendor build.prop
BUILD_FINGERPRINT := POCO/evergo_p_in/evergo:12/SP1A.210812.016/V13.0.7.0.SGBINXM:user/release-keys
